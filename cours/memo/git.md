# Git

Ce document n'a pas pour but d'expliquer comment fonctionne `git` mais a plutôt pour vocation de vous décrire dans un document clair et court comment utiliser un dépôt `git`.

> Sur Windows je vous recommande de télécharger [git-scm

## Step 1 : Création du dépôt git

Pour cela, rendez-vous sur une plateforme Git comme Gitlab ou Github ou autres.

> Les exemples seront donnés avec Gitlab dans ce document.

Dans Gitlab :

- connectez-vous avec votre utilisateur
- créez un nouveau dépôt
- repérez le bouton bleu "Clone" (voir l'image ci-dessous) 
- copier l'adresse en **SSH** (pas celle en HTTPS)

![Bouton clone](./pics/gitlab_clone.png)

## Step 2 : Récupérer localement le dépôt

Ouvrez un terminal sur votre machine et exécutez un `clone` : c'est la commande qui permet de récupérer locale, sur votre PC, le dossier créé sur Gitlab.

```bash
# Syntaxe
$ git clone <URL_SSH_QUE_VOUS_AVEZ_COPIEE>

# Par exemple
$ git clone git@gitlab.com:it4lik/cesi1-linux-2021.git

# Une fois effectué, cela a créé un dossier qui porte le nom de notre dépôt
# Il est dans le dossier courant
$ ls
cesi1-linux-2021
```

Voilà, c'est fait : vous avez le dossier en local, vous avez le dépôt git en local.

## Step 3 : Prêt à bosser !

Bah on est tout bons ! Une fois qu'on a le dossier, faites vos modifs dedans :

- créer des fichiers
- modifier des fichiers
- etc etc, c'est un simple dossier, vous faites ce que vous voulez !

Une fois vos modifs effectuées, vous voulez les envoyer, les "pousser", sur le serveur central.

Pour ça : 

- `git add` sur chaque fichier ajouté/modifié
- `git commit` pour valider tous vos changements, et leur donner un nom
- `git push` pour envoyer le contenu de votre commit sur le serveur central

Exemple :

```bash
# On modifie un fichier appelé "test_file"
$ echo "test" > test_file

# git status pour voir que git a vu votre modif

# J'indique à git que j'ai modifié le fichier
$ git add test_file

# git status

# Je modifie un autre fichier
$ echo "yolo" > test_file_2

# git status

# J'indique à git que j'ai modifié un deuxième fichier
$ git add test_file_2

# git status

# On valide ces deux changements
# Un message de commit explicite qui décrit les changements effectués
$ git commit -m "add two test files"

# git status, sérieux spammez-le, il vous dit tout !

# On envoie sur le serveur central !
$ git push
```
