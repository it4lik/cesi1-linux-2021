# TP2 : Un service entouré d'un écosystème riche

Sur ce TP2, nous allons nous concentrer sur un aspect du job de l'administrateur système : **la fourniture de service**.

Ce service ne sera pas un bête serveur simplement pour du test. En effet, on va aller un peu plus loin et setup une vraie application, réellement utilisée, aussi bien par des particuliers que des pros : NextCloud.

Autour de cet application, nous allons construire un écosystème riche :

- sécurité
- reverse proxy
- chiffrement
- monitoring
- backup

## Sommaire

- [TP2 : Un service entouré d'un écosystème riche](#tp2--un-service-entouré-dun-écosystème-riche)
  - [Sommaire](#sommaire)
  - [Intro](#intro)
  - [Prérequis](#prérequis)
  - [Suite du TP](#suite-du-tp)

## Intro

**[NextCloud](https://github.com/nextcloud/server) est un outil, qui, une fois en place, présente des fonctionnalités très similaires à Google Drive ou DropBox.** C'est ce qu'on appelle vulgairement un "cloud".

> J'aime pô le terme "cloud" car on l'utilise en informatique technique pour désigner autre chose, on y reviendra très certainement quand on abordera les sujets autour du DevOps en fin de semaine.

---

On ne va **clairement pas** se limiter à une install de NextCloud. J'ai choisi cette solution car :

- elle a une vraie utilité, et elle est vraiment utilisée dans le monde réel
- libre & open-source
- elle est composée de deux services :
  - serveur Web
  - serveur de base de données
  - donc bon un très bon exercice dans le cadre du TP
- oh et puis l'interface est jolie :D

> Oui ! Encore du serveur web ! **C'est pas moi, c'est le 21ème siècle qui veut ça** : de plus en plus de choses passent par du web, et de moins en moins par des applications lourdes.

---

**Une fois en place, on enrichira l'écosystème autour de notre application.** On peut regrouper les améliorations que l'on va effectuer sur notre install en trois aspects :

- **respect du principe du moindre privilège**
  - configuration de `sudo`
  - utilisation d'utilisateurs applicatifs
  - firewall configuré
  - ce sera fait au fur et à mesure des installations
- **sécurité autour des outils déjà mis en place**
  - sécurisation du serveur SSH
  - sécurisation du serveur Web
  - on le fera après coup : une fois en place, on affinera la configuration
- **maintien en condition opérationnelle**
  - monitoring
  - backup
  - là aussi, ce sera fait dans un second temps

**Mais avant ça, on va préparer nos machines !**

## Prérequis

Pour ce TP, on va donc avoir besoin de deux machines virtuelles 🖥️ :

| Machine        | IP             | Service                              |
|----------------|----------------|--------------------------------------|
| `web.tp2.cesi` | `10.2.1.11/24` | Serveur Web : Apache                 |
| `db.tp2.cesi`  | `10.2.1.12/24` | Serveur de base de données : MariaDB |

> Pour la conf réseau au sein de Rocky, j'ai écrit un mémo : [iciiii](../../cours/memo/rocky_network.md).

Vous déroulerez la **📝checklist📝** suivante, sur les deux machines :

- [x] la VM a un accès internet
  - `ping 1.1.1.1` fonctionnel
  - résolution de nom fonctionnelle : `ping cesi.fr` fonctionnel
- [x] SELinux est désactivé
  - la commande `sestatus` doit retourner : `Current Mode: permissive`
  - la commande `sestatus` doit retourner : `Mode from config file: permissive`
- [x] vous avez une connexion SSH fonctionnelle vers la VM
  - avec un échange de clé
- [x] la machine est nommée
  - fichier `/etc/hostname`
  - commande `hostname`

> La checklist ne doit pas figurer dans le rendu, elle doit simplement être effectuée sur toutes les machines.

## Suite du TP

Le déroulement du TP se fait ensuite en 3 parties :

- [Partie 1 : Mise en place de la solution](./part1.md)
- [Partie 2 : Sécurisation](./part2.md)
- [Partie 3 : Maintien en condition opérationnelle](./part3.md)
