# I. Préparation de la machine

> Tant que rien n'a été indiqué à ce sujet, vous pouvez utilisez directement l'utilisateur `root` pour effectuer les opération.

## Sommaire

- [I. Préparation de la machine](#i-préparation-de-la-machine)
  - [Sommaire](#sommaire)
  - [1. Réseau](#1-réseau)
    - [A. IP statique](#a-ip-statique)
    - [B. DNS](#b-dns)
    - [C. Hostname](#c-hostname)
  - [2. Utilisateurs](#2-utilisateurs)
  - [3. SSH](#3-ssh)
  - [4. SELinux](#4-selinux)

## 1. Réseau

### A. IP statique

Au sein des systèmes dérivés de RedHat comme Rocky, la configuration IP se fait dans le dossier `/etc/sysconfig/network-scripts/`. Le concept est de créer un fichier pour chaque interface réseau.

Les fichiers s'appellent `ifcfg-<INTERFACE>`, par exemple `ifcfg-enp0s3` pour l'interface `enp0s3`.

> Vous pouvez utiliser la commande `ip a` pour voir la liste des cartes réseau de la machine, leur nom, et leur configuration actuelle.

La syntaxe des fichiers est la suivante :

```bash
NAME=enp0s8          # nom de l'interface
DEVICE=enp0s8        # nom de l'interface
BOOTPROTO=static     # définition statique de l'IP (par opposition à DHCP)
ONBOOT=yes           # la carte s'allumera automatiquement au boot de la machine
IPADDR=<IP_CHOISIE>  # adresse IP choisie
NETMASK=<MASK>       # masque choisi au format binaire. Ex : 255.255.255.0
```

Une fois qu'un fichier de conf a été modifié, il faut exécuter les commandes suivantes pour que les changements prennent effet :

```bash
# Indiquer au système que la conf a été modifiée
$ nmcli con reload

# Restart l'interface concernée
$ nmcli con up enp0s8
```

🌞 **Définissez l'IP `10.1.1.20/24` à votre machine virtuelle.**

- repérez le nom de votre carte réseau avec `ip a`
- créez ou modifiez le fichier de configuration de l'interface
- vérifiez avec la commande `ip a` que votre config a bien pris effet
- votre PC doit pouvoir joindre la machine virtuelle
  - vérifiez le en essayant de ping la machine virtuelle depuis votre PC

> Il sera probablement nécessaire d'effectuer une configuration dans votre hyperviseur, qui dépendra de l'hyperviseur que vous utilisez. N'hésitez pas à me contacter si vous avez besoin d'aide là dessus :)

### B. DNS

🌞 **Définissez l'utilisation d'un serveur DNS**

- la configuration se fait dans le fichier de configuration d'interface
- ajoutez la clause `DNS1=IP`
- utilisez le serveur DNS public 1.1.1.1

> N'oubliez pas : `nmcli con reload` puis `nmcli con up <INTERFACE>` quand vous modifiez le fichier de conf d'interface.

🌞 **Vérifiez que vous avez de la résolution de noms**

- en exécutant `ping google.com` par exemple

### C. Hostname

Pour définir un hostname à une machine GNU/Linux, il faut faire deux opérations :

```bash
# 1. Changer le nom d'hôte immédiatement (temporaire)
sudo hostname <NEW_HOSTNAME>
# par exemple
sudo hostname node1.tp1.cesi

# 2. Définir un nom d'hôte quand la machine s'allume (permanent)
# écriture du nom d'hôte dans le fichier /etc/hostname (avec nano par exemple) :
$ sudo nano /etc/hostname
# OU, sans nano, en une seule commande :
$ `echo 'node1.tp1.cesi' | sudo tee /etc/hostname

# 3. Pour consulter votre nom d'hôte actuel
$ hostname
```

🌞 **Changez le hostname de la machine**

- définissez lui `node1.tp1.cesi` comme nom
- assuez-vous que c'est bon
  - en tapant la commande `hostname`
  - en vous déconnectant/reconnectant, la modif est visible dans le prompt de l'invit de commande une fois reconnecté

## 2. Utilisateurs

Dans cette section, on va créer un user et le rendre admin sur la machine.

Dans les systèmes GNU/Linux, "être admin" ça signifie avoir le droit de prendre temporairement les droits de root pour exécuter telle commande ou tel programme.

En ligne de commande, on peut **utiliser `sudo` afin de prendre temporairement l'identité d'un autre utilisateur** et exécuter une commande.  
Pour définir quel utilisateur a le droit de prendre l'identité de quel autre utilisateur afin d'exécuter telle ou telle commande, **la conf de `sudo` quoi, c'est dans le fichier `/etc/sudoers` que ça se passe.**

Le but pour nous va être de créer un nouvel utilisateur, et faire en sorte qu'il puisse exécuter n'importe quelle commande en tant que `root`.

Dans les systèmes dérivés de RedHat comme Rocky, il existe par défaut un groupe `wheel`. Tous les membres de ce groupe ont le droit d'exécuter n'importe quelle commande en tant que `root`. C'est le groupe des admins par défaut :)

🌞 **Repérez dans le fichier `/etc/sudoers` la ligne qui donne tous les droits au groupe `wheel`**

- vous **DEVEZ** utiliser la commande `visudo` pour modifier le fichier `/etc/sudoers`
  - cette commande ne sert qu'à ça
  - juste `visudo` sans options ni arguments
  - elle permet de vérifier en quittant le fichier qu'il ne contient pas d'erreurs de syntaxe
  - en effet, une erreur dans ce fichier de syntaxe serait très grave : vous n'auriez plus d'accès aux droits `root`

> Vous pouevez facilement mettre en évidence une ligne unique dans un fichier avec la syntaxe `cat FICHIER | grep STRING`. La commande `grep` sert de filtre. Cela permet donc d'afficher le contenu du `FICHIER` mais uniquement les lignes qui contiennent le mot `STRING`.

🌞 **Créez un nouvel utilisateur**

- avec la commande `useradd`
  - comme avec toutes les commandes : consultez le manuel avec `man useradd` ou une version condensée avec `useradd --help`
- appelez-le comme vous voulez
- contraintes :
  - il doit appartenir au groupe `wheel`
  - il doit avoir un répertoire personnel (homedir) dans `/home/<USER>`
  - il doit avoir un mot de passe
- une fois créé, connectez-vous avec, et vérifiez que vous pouvez exécuter des commandes avec `sudo`

## 3. SSH

SSH est un protocole qui permet de contrôler un shell dans une machine, à travers le réseau, à distance. On l'utilise quotidiennement pour administrer des serveurs GNU/Linux.

Par défaut sous Rocky, le service `sshd` est déjà installé et démarré, et le firewall autorise déjà les connexions sur le port par défaut de SSH : 22/TCP.

🌞 **Connectez-vous en SSH à la machine virtuelle**

> Je veux pas voir Putty ou autres, vous avez la commande `ssh` dans votre PowerShellsi vous êtes sous Windows. La syntaxe est simple : `ssh USER@IP` pour vous connectez en tant que `USER` sur la machine qui porte l'IP `IP`.

## 4. SELinux

SELinux (pour Security Enhanced Linux) est un outil qui permet d'augmenter le niveau de sécurité des systèmes GNU/Linux. Il est présent par défaut sur les systèmes dérivés de RedHat.

Il est très fastidieux de le configurer, nous allons simplement le désactiver pour nos TPs, afin qu'il ne bloque pas nos opérations

🌞 **Désactivez SELinux**

- afficher son état : `sestatus`
  - le `Current Mode` correspond à son état actuel (`enforcing` actuellemment)
  - le `Mode from config file` correspond à l'état demandé par le fichier de config (`enforcing` actuellement)
- désactivation immédiate mais temporaire : `setenforce 0`
- désactivation permanente : modification du fichier `/etc/selinux/config`
  - vous devrez demander le mode `permissive` à la place du mode `enforcing`
- vérifiez avec `sestatus` que le `Current Mode` et le `Mode from config file` sont sur `permissive` après vos modifs
