# TP1 : On se met dans le bain

On commence avec un TP tranquille pour manipuler la ligne de commandes et l'environnement GNU/Linux.

Au menu :

- préparation de la machine en vue d'une utilisation en tant que serveur
- gestion de services (SSH, serveur Web, création de service)

On en profitera pour voir plusieurs commandes élémentaires tout du long, ainsi que des éléments de configuration récurrents sur les systèmes GNU/Linux. **On va voir, notamment** :

- revue des commandes élémentaires
- gestion d'utilisateurs et de permissions
- gestion de services
- gestion de la stack réseau
- serveur SSH

**Le TP se divise en deux parties :**

- [I. Préparation de la machine](./part1.md)
- [II. Gestion de services](./part2.md)
- [III. Bonus](./part3.md)
